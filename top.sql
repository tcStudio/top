-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016-01-19 10:37:23
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `top`
--

-- --------------------------------------------------------

--
-- 表的结构 `top_auth`
--

DROP TABLE IF EXISTS `top_auth`;
CREATE TABLE IF NOT EXISTS `top_auth` (
  `auth_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `auth_name` varchar(20) NOT NULL COMMENT '名称',
  `auth_pid` smallint(6) unsigned NOT NULL COMMENT '父id',
  `auth_c` varchar(32) NOT NULL DEFAULT '' COMMENT '控制器',
  `auth_a` varchar(32) NOT NULL DEFAULT '' COMMENT '操作方法',
  `auth_path` varchar(32) NOT NULL COMMENT '全路径',
  `auth_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '级别',
  `auth_menu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否显示在左侧菜单0为否1为是',
  `auth_sort` smallint(6) unsigned NOT NULL DEFAULT '10' COMMENT '权限排序',
  PRIMARY KEY (`auth_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- 转存表中的数据 `top_auth`
--

INSERT INTO `top_auth` (`auth_id`, `auth_name`, `auth_pid`, `auth_c`, `auth_a`, `auth_path`, `auth_level`, `auth_menu`, `auth_sort`) VALUES
(1, '用户管理', 0, '', '', '1', 0, 1, 10),
(2, '用户列表', 1, 'Manager', 'showlist', '1-2', 1, 1, 0),
(3, '角色管理', 1, 'Role', 'showlist', '1-3', 1, 1, 0),
(4, '权限管理', 7, 'Authority', 'showlist', '7-4', 1, 1, 0),
(5, '仓库管理', 0, '', '', '5', 0, 1, 9),
(6, '生产计划管理', 0, '', '', '6', 0, 0, 0),
(7, '系统管理', 0, '', '', '7', 0, 1, 0),
(8, '部门管理', 1, 'Bumen', 'showlist', '1-8', 1, 1, 0),
(9, '货物信息列表', 5, 'Goods', 'showlist', '5-9', 1, 1, 0),
(10, '货物单位列表', 5, 'Goods', 'dwlist', '5-10', 1, 1, 0),
(11, '货物入库', 5, 'Wareroom', 'add_to_w', '5-11', 1, 1, 0),
(12, '出入库待审列表', 5, 'Wareroom', 'check_l', '5-12', 1, 1, 0),
(13, '货物库存列表', 5, 'Wareroom', 'wlist', '5-13', 1, 1, 0),
(14, '仓库进出记录', 5, 'Wareroom', 'wlog', '5-14', 1, 1, 0),
(15, '添加权限', 7, 'Authority', 'addAuth', '7-15', 1, 0, 0),
(16, '修改权限', 7, 'Authority', 'updateAuth', '7-16', 1, 0, 0),
(17, '添加部门', 1, 'Bumen', 'add', '1-17', 1, 0, 0),
(18, '编辑部门', 1, 'Bumen', 'update', '1-18', 1, 0, 0),
(19, '删除部门', 1, 'Bumen', 'delete', '1-19', 1, 0, 0),
(20, '录入货物信息', 5, 'Goods', 'add', '5-20', 1, 0, 0),
(21, '修改货物信息', 5, 'Goods', 'update', '5-21', 1, 0, 0),
(22, '删除货物信息', 5, 'Goods', 'delete', '5-22', 1, 0, 0),
(23, '添加货物单位', 5, 'Goods', 'dwadd', '5-23', 1, 0, 0),
(24, '编辑货物单位', 5, 'Goods', 'update_dw', '5-24', 1, 0, 0),
(25, '删除货物单位', 5, 'Goods', 'del_dw', '5-25', 1, 0, 0),
(26, '添加用户', 1, 'Manager', 'add', '1-26', 1, 0, 0),
(27, '编辑用户', 1, 'Manager', 'update', '1-27', 1, 0, 0),
(28, '删除用户', 1, 'Manager', 'delete', '1-28', 1, 0, 0),
(29, '添加角色', 1, 'Role', 'add', '1-29', 1, 0, 0),
(30, '编辑角色', 1, 'Role', 'update', '1-30', 1, 0, 0),
(31, '更改角色排序', 1, 'Role', 'updateSort', '1-31', 1, 0, 0),
(32, '分配权限', 1, 'Role', 'distribute', '1-32', 1, 0, 0),
(33, '删除角色', 1, 'Role', 'delete', '1-33', 1, 0, 0),
(34, '入库申请', 5, 'Wareroom', 'add', '5-34', 1, 0, 0),
(35, '审核出入库', 5, 'Wareroom', 'check_d', '5-35', 1, 0, 0),
(36, '出库申请', 5, 'Wareroom', 'out_w', '5-36', 1, 0, 0),
(37, '仓库添加', 5, 'Category', 'showlist', '5-37', 1, 1, 100),
(38, '添加仓库', 5, 'Category', 'add', '5-38', 1, 0, 99),
(39, '编辑仓库', 5, 'Category', 'update', '5-39', 1, 0, 98),
(40, '删除仓库', 5, 'Category', 'delete', '5-40', 1, 0, 97),
(41, '网盘管理', 0, '', '', '41', 0, 1, 3),
(42, '我的网盘', 41, 'Disk', 'index', '41-42', 1, 1, 5),
(43, '查看共享文件', 41, 'Disk', 'slist', '41-43', 1, 1, 4),
(44, '新建文件夹', 41, 'Disk', 'mkdir', '41-44', 1, 0, 0),
(45, '共享文件', 41, 'Disk', 'sharef', '41-45', 1, 0, 0),
(46, '下载文件', 41, 'Disk', 'download', '41-46', 1, 0, 0),
(47, '删除文件', 41, 'Disk', 'delf', '41-47', 1, 0, 0),
(48, '删除文件夹', 41, 'Disk', 'deld', '41-48', 1, 0, 0),
(49, '上传文件', 41, 'Disk', 'upload', '41-49', 1, 0, 0),
(50, '库存预警', 5, 'Wareroom', 'yjlist', '5-50', 1, 1, 99),
(51, '货物信息导出页', 5, 'Excel', 'get_g_l', '5-51', 1, 0, 0),
(52, '货物库存导出页', 5, 'Excel', 'get_c_l', '5-52', 1, 0, 0),
(53, '货物信息导入页', 5, 'Excel', 'import_g', '5-53', 1, 0, 0),
(54, '货物信息导入处理', 5, 'Excel', 'goods_import', '5-54', 1, 0, 0),
(55, '货物库存导入页', 5, 'Excel', 'import_c', '5-55', 1, 0, 0),
(56, '货物库存导入处理', 5, 'Excel', 'count_import', '5-56', 1, 0, 0),
(57, '货物信息导出处理', 5, 'Excel', 'goods_export', '5-57', 1, 0, 0),
(58, '货物库存导出处理', 5, 'Excel', 'count_export', '5-58', 1, 0, 0),
(59, '生成excel', 5, 'Excel', 'getExcel', '5-59', 1, 0, 0),
(60, '出入库原因列表', 5, 'Waction', 'showlist', '5-60', 1, 1, 0),
(61, '添加出入库原因', 5, 'Waction', 'add', '5-61', 1, 0, 0),
(62, '修改出入库原因', 5, 'Waction', 'update', '5-62', 1, 0, 0),
(63, '删除出入库原因', 5, 'Waction', 'del', '5-63', 1, 0, 0),
(64, '修改密码', 1, 'Manager', 'password', '1-64', 1, 0, 0),
(65, '货物调拨待审列表', 5, 'TransferGoods', 'showlist', '5-65', 1, 1, 0),
(66, '货物调拨登记', 5, 'TransferGoods', 'transfer', '5-66', 1, 0, 0),
(67, '货物调拨审核', 5, 'TransferGoods', 'check', '5-67', 1, 0, 0),
(68, '删除货物调拨申请记录', 5, 'TransferGoods', 'delete', '5-68', 1, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `top_bumen`
--

DROP TABLE IF EXISTS `top_bumen`;
CREATE TABLE IF NOT EXISTS `top_bumen` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `pid` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '上级部门ID',
  `name` varchar(30) NOT NULL COMMENT '部门名称',
  `fzr` varchar(30) NOT NULL COMMENT '部门负责人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `top_disk`
--

DROP TABLE IF EXISTS `top_disk`;
CREATE TABLE IF NOT EXISTS `top_disk` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(6) unsigned NOT NULL COMMENT '父ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '文件夹名',
  `uid` int(6) unsigned NOT NULL COMMENT '所属用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `top_files`
--

DROP TABLE IF EXISTS `top_files`;
CREATE TABLE IF NOT EXISTS `top_files` (
  `fid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `did` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '所在文件夹ID',
  `uid` int(6) unsigned NOT NULL COMMENT '所属用户ID',
  `fname` varchar(150) NOT NULL COMMENT '文件名',
  `path` varchar(255) NOT NULL COMMENT '文件路径',
  `filetype` varchar(30) NOT NULL COMMENT '文件类型',
  `filesize` int(11) unsigned NOT NULL COMMENT '文件大小',
  `isshare` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为共享文件',
  `addtime` int(11) unsigned NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `top_goods`
--

DROP TABLE IF EXISTS `top_goods`;
CREATE TABLE IF NOT EXISTS `top_goods` (
  `g_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `g_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '货物编号',
  `g_name` varchar(255) NOT NULL COMMENT '货物名称',
  `g_dw_id` int(6) unsigned NOT NULL COMMENT '货物单位',
  `g_yjs` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '库存预警数',
  `g_zl` float(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '货物重量',
  `g_fzr` varchar(30) NOT NULL COMMENT '负责人',
  `g_bz` varchar(255) NOT NULL COMMENT '备注信息',
  `g_addtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `g_updatetime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`g_id`),
  KEY `g_name` (`g_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `top_goods_cate`
--

DROP TABLE IF EXISTS `top_goods_cate`;
CREATE TABLE IF NOT EXISTS `top_goods_cate` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '仓库ID',
  `pid` int(6) unsigned NOT NULL COMMENT '上级仓库ID',
  `name` varchar(30) NOT NULL COMMENT '仓库名称',
  `fzr` varchar(30) NOT NULL COMMENT '仓库负责人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `top_goods_count`
--

DROP TABLE IF EXISTS `top_goods_count`;
CREATE TABLE IF NOT EXISTS `top_goods_count` (
  `cid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '库存ID',
  `bid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '仓库id',
  `gid` int(10) unsigned NOT NULL COMMENT '货物ID',
  `gcount` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '库存数',
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `top_goods_dw`
--

DROP TABLE IF EXISTS `top_goods_dw`;
CREATE TABLE IF NOT EXISTS `top_goods_dw` (
  `dw_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT COMMENT '单位ID',
  `dw_name` varchar(30) NOT NULL COMMENT '单位名称',
  `dw_bz` varchar(255) NOT NULL COMMENT '单位备注',
  PRIMARY KEY (`dw_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `top_goods_log`
--

DROP TABLE IF EXISTS `top_goods_log`;
CREATE TABLE IF NOT EXISTS `top_goods_log` (
  `lid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `gid` int(11) unsigned NOT NULL COMMENT '货物ID',
  `aid` tinyint(1) unsigned NOT NULL COMMENT '1为入库2为出库',
  `bid` int(11) unsigned NOT NULL COMMENT '所属部门ID',
  `gcount` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '进出数量',
  `action` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '操作原因ID',
  `doer` varchar(30) NOT NULL COMMENT '操作人',
  `ischeck` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '审核状态0为未审1为通过2为拒绝',
  `checker` varchar(30) NOT NULL COMMENT '审核人',
  `d_time` int(11) unsigned NOT NULL COMMENT '申请时间',
  `c_time` int(11) unsigned NOT NULL COMMENT '审核时间',
  PRIMARY KEY (`lid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `top_manager`
--

DROP TABLE IF EXISTS `top_manager`;
CREATE TABLE IF NOT EXISTS `top_manager` (
  `mg_id` int(11) NOT NULL AUTO_INCREMENT,
  `mg_name` varchar(32) NOT NULL,
  `mg_pwd` varchar(32) NOT NULL,
  `mg_last_time` int(11) unsigned NOT NULL DEFAULT '0',
  `mg_add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  `mg_role_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `mg_bm_id` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '所在部门ID',
  `mg_c_id` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '指定仓库',
  PRIMARY KEY (`mg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `top_manager`
--

INSERT INTO `top_manager` (`mg_id`, `mg_name`, `mg_pwd`, `mg_last_time`, `mg_add_time`, `mg_role_id`, `mg_bm_id`, `mg_c_id`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1453169573, 1446951182, 1, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `top_role`
--

DROP TABLE IF EXISTS `top_role`;
CREATE TABLE IF NOT EXISTS `top_role` (
  `role_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) NOT NULL COMMENT '角色名称',
  `role_auth_ids` varchar(128) NOT NULL DEFAULT '0' COMMENT '权限ids,1,2,5',
  `role_auth_ac` text COMMENT '控制器-操作方法',
  `role_sort` smallint(6) unsigned NOT NULL DEFAULT '10' COMMENT '角色排序',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `top_transfer_log`
--

DROP TABLE IF EXISTS `top_transfer_log`;
CREATE TABLE IF NOT EXISTS `top_transfer_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `gid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '货物ID',
  `from_w` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '调出仓库ID',
  `to_w` int(10) unsigned NOT NULL COMMENT '调入仓库ID',
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '调拨数量',
  `doer` varchar(30) NOT NULL DEFAULT '' COMMENT '制作人',
  `ischeck` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '审核状态0为未审1为通过2为拒绝',
  `checker` varchar(30) NOT NULL DEFAULT '' COMMENT '审核人',
  `d_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '申请时间',
  `c_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '审核时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `top_w_action`
--

DROP TABLE IF EXISTS `top_w_action`;
CREATE TABLE IF NOT EXISTS `top_w_action` (
  `a_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '出入库原因ID',
  `a_mark` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '出入库标识2为出库1为入库',
  `a_info` varchar(255) NOT NULL DEFAULT '' COMMENT '出入库原因描述',
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `top_w_action`
--

INSERT INTO `top_w_action` (`a_id`, `a_mark`, `a_info`) VALUES
(1, 1, '调拨');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
