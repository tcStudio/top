<?php
return array(
	//'配置项'=>'配置值'
	'SHOW_PAGE_TRACE'   =>  false,
    'DB_TYPE'               =>  'mysql',     // 数据库类型
    'DB_HOST'               =>  'localhost', // 服务器地址
    'DB_NAME'               =>  'top',          // 数据库名
    'DB_USER'               =>  'root',      // 用户名
    'DB_PWD'                =>  'root',          // 密码
    'DB_PORT'               =>  '3306',        // 端口
    'DB_PREFIX'             =>  'top_',    // 数据库表前缀
    
    'MODULE_ALLOW_LIST' => array('Home'),
    'DEFAULT_MODULE'    => 'Home',
    'URL_HTML_SUFFIX'   =>  '',  // URL伪静态后缀设置
);