<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>出入库操作原因列表</title>
	<script type="text/javascript" src="/Public/hdjs/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="/Public/hdjs/hdjs.css"/>
    <script type="text/javascript" src="/Public/hdjs/hdjs.min.js"></script>
	<link href="/Public/css/mine.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<div class="div_head">
		<span>
			<span style="float: left;">当前位置是：仓库管理 >></span><span>出入库操作原因</span>
			<span style="float: right; margin-right: 8px; font-weight: bold;">
				<a style="text-decoration: none;" href="<?php echo U('add');?>">【添加操作原因】</a>
			</span>
		</span>
	</div>
	<div style="font-size: 13px; margin: 10px 5px;padding:20px;">
		<table class="hd-table hd-table-list">
			<thead>
				<tr style="font-weight: bold;">
					<td>编号</td>
					<td>出入行为</td>
					<td>出入库原因描述</td>
					<td>操作</td>
				</tr>
			</thead>
			<tbody>
				<?php if(is_array($alist)): $i = 0; $__LIST__ = $alist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr id="product1">
					<td><?php echo ($vo["a_id"]); ?></td>
					<td><?php if($vo['a_mark'] == 1): ?><a href="<?php echo U('showlist',array('aid' =>$vo['a_mark']));?>">入库</a>
						<?php else: ?>
							<a href="<?php echo U('showlist',array('aid' =>$vo['a_mark']));?>"><span class="red">出库</span></a><?php endif; ?></td>
					<td><?php echo ($vo["a_info"]); ?></td>
                    <td><a href="<?php echo U('update',array('id' => $vo['a_id']));?>">修改</a>|
					<a onclick="if (confirm('确定要删除吗？')) return true; else return false;" href="<?php echo U('del',array('id' => $vo['a_id']));?>">删除</a></td>
				</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="hd-page">
			<?php echo ($page); ?>
		</div>
	</div>
</body>
</html>