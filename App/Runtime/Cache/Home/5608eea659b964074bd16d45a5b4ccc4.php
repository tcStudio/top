<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>仓库进出记录</title>
	<script type="text/javascript" src="/Public/hdjs/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="/Public/hdjs/hdjs.css"/>
    <script type="text/javascript" src="/Public/hdjs/hdjs.min.js"></script>
	<link href="/Public/css/mine.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<div class="div_head">
		<span>
			<span style="float: left;">当前位置是：仓库管理 >></span><span>仓库进出记录</span>
			<span style="float: right; margin-right: 8px; font-weight: bold;">
				<a style="text-decoration: none;" href="<?php echo U('Excel/get_l_l');?>">【导出进出记录】</a>
			</span>
		</span>
	</div>
	<div style="font-size: 13px; margin: 10px 5px;padding:0 20px;">
		<p class="hd-title-header">按货物名称查找</p>
		<div style="padding:15px;">
		<form method="get" action="/index.php/Home/Wareroom/wlog">
		<input type="hidden" name="p" value="1" />
		<input type="hidden" name="b_id" value="0" />
		输入货物名称：<input type="text" name="g_name" value="<?php echo I('get.g_name'); ?>" class="hd-w250 hd-h40" />
		<input type="submit" value="查找" class="hd-btn hd-btn-primary"/>
		</form>
		</div>
		<p class="hd-title-header">按所属仓库查找</p>
		<dl class="w_bm_l" >
			<?php if(is_array($c_list)): $i = 0; $__LIST__ = $c_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$b): $mod = ($i % 2 );++$i; if($b['level'] == 1): ?><dt><a href="<?php echo U('wlog',array('b_id'=>$b['id']));?>"><?php echo ($b["name"]); ?></a></dt>
				<?php else: ?>
				<dd><a href="<?php echo U('wlog',array('b_id'=>$b['id']));?>"><?php echo ($b["name"]); ?></a></dd><?php endif; endforeach; endif; else: echo "" ;endif; ?>
		</dl>
	</div>
	<div style="font-size: 13px; margin: 10px 5px;padding:0 20px;clear:both" class="hd-menu-list">
		<div class="hd-page">
			<?php echo ($page); ?>
		</div>
		<table class="hd-table hd-table-list">
			<thead>
				<tr style="font-weight: bold;">
					<td>货物名称</td>
					<td class="hd-w80">所属仓库</td>
					<td class="hd-w50">货物单位</td>
					<td class="hd-w80">货物单个重量</td>
					<td class="hd-w50">出入行为</td>
					<td>出入原因</td>
					<td class="hd-w50">出入数量</td>
					<td class="hd-w50">审核状态</td>
					<td class="hd-w80">审核时间</td>
					<td class="hd-w50">申请人</td>
					<td class="hd-w50">审核人</td>
					<td class="hd-w50">操作</td>
				</tr>
			</thead>
			<tbody>
				<?php if(is_array($log_l)): $i = 0; $__LIST__ = $log_l;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr id="product1">
					<td><?php echo ($v["g_name"]); ?></td>
					<td><?php echo ($v["name"]); ?></td>
					<td><?php echo ($v["dw_name"]); ?></td>					
					<td><?php if($v['g_zl'] == 0 ): ?>未知<?php else: echo ($v["g_zl"]); ?>kg<?php endif; ?></td>
					<td><?php if($v['aid'] == 1): ?>入库<?php else: ?>出库<?php endif; ?></td>
					<td><?php echo ($v["a_info"]); ?></td>
					<td>
						<?php if($v['aid'] == 1): echo ($v["gcount"]); ?>
						<?php else: ?>
							<span class="red"><?php echo ($v["gcount"]); ?></span><?php endif; ?>
					</td>
					<td>
						<?php if($v['ischeck'] == 1): ?><a href="<?php echo U('wlog',array('b_id'=>$v['ischeck']));?>">已通过</a>
						<?php else: ?>
							<a href="<?php echo U('wlog',array('b_id'=>$v['ischeck']));?>"><span class="red">已拒绝</span></a><?php endif; ?>
					</td>
					<td><?php echo (date("Y-m-d",$v["c_time"])); ?></td>
					<td><?php echo ($v["doer"]); ?></td>
                    <td><?php echo ($v["checker"]); ?></td>
                    <td><a onclick="if (confirm('确定要删除吗？')) return true; else return false;" href="<?php echo U('delete',array('lid' => $v['lid']));?>">删除</a></td>
				</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="hd-page">
			<?php echo ($page); ?>
		</div>
	</div>
</body>
</html>