<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>货物库存预警列表</title>
	<script type="text/javascript" src="/Public/hdjs/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="/Public/hdjs/hdjs.css"/>
    <script type="text/javascript" src="/Public/hdjs/hdjs.min.js"></script>
	<link href="/Public/css/mine.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<div class="div_head">
		<span>
			<span style="float: left;">当前位置是：仓库管理 >></span><span>货物库存预警列表</span>
		</span>
	</div>
	<div style="font-size: 13px; margin: 10px 5px;padding:0 20px;">
		<p class="hd-title-header">按货物名称查找</p>
		<div style="padding:15px;">
		<form method="get" action="/index.php/Home/Wareroom/yjlist">
		<input type="hidden" name="p" value="1" />
		<input type="hidden" name="b_id" value="0" />
		输入货物名称：<input type="text" name="g_name" value="<?php echo I('get.g_name'); ?>" class="hd-w250 hd-h40" />
		<input type="submit" value="查找" class="hd-btn hd-btn-primary"/>
		</form>
		</div>
		<p class="hd-title-header">按所属仓库查找</p>
		<dl class="w_bm_l" >
			<?php if(is_array($c_list)): $i = 0; $__LIST__ = $c_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$b): $mod = ($i % 2 );++$i; if($b['level'] == 1): ?><dt><a href="<?php echo U('yjlist',array('b_id'=>$b['id']));?>"><?php echo ($b["name"]); ?></a></dt>
				<?php else: ?>
				<dd><a href="<?php echo U('yjlist',array('b_id'=>$b['id']));?>"><?php echo ($b["name"]); ?></a></dd><?php endif; endforeach; endif; else: echo "" ;endif; ?>
		</dl>
	</div>
	<div style="font-size: 13px; margin: 10px 5px;padding:0 20px;clear:both" class="hd-menu-list">
		<div class="hd-page">
			<?php echo ($page); ?>
		</div>
		<table class="hd-table hd-table-list">
			<thead>
				<tr style="font-weight: bold;">
					<td class="hd-w30">编号</td>
					<td class="">货物名称</td>
					<td class="hd-w100">货物库存预警数</td>
					<td class="hd-w100">货物当前库存数量</td>
					<td class="hd-w50">货物单位</td>
					<td class="hd-w100">货物单个重量</td>
					<td class="hd-w50">所属仓库</td>
					<td class="hd-w50">操作</td>
				</tr>
			</thead>
			<tbody>
				<?php if(is_array($g_list)): $i = 0; $__LIST__ = $g_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr id="product1">
					<td><?php echo ($vo["g_id"]); ?></td>
					<td><?php echo ($vo["g_name"]); ?></td>
					<td><?php echo ($vo["g_yjs"]); ?></td>
					<td><span class="red"><?php echo ($vo["gcount"]); ?></span></td>
					<td><?php echo ($vo["dw_name"]); ?></td>
					<td><?php if($vo['g_zl'] == 0 ): ?>未知<?php else: echo ($vo["g_zl"]); ?>kg<?php endif; ?></td>
					<td><?php echo ($vo["name"]); ?></td>
                    <td><a href="<?php echo U('out_w',array('gid' => $vo['gid']));?>" class="hd-btn hd-btn-primary hd-btn-sm" >调出</a></td>
				</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="hd-page">
			<?php echo ($page); ?>
		</div>
	</div>
</body>
</html>