<?php
namespace Home\Model;
use Think\Model;
class TransferLogModel extends Model{
	protected $tableName = 'transfer_log';
	protected $_auto = array (
		array('doer','getdoer',1,'callback'),
		array('checker','getdoer',2,'callback'),
		array('ischeck','0'),
		array('d_time','time',1,'function'),
		array('c_time','time',2,'function'), 
	);

    protected $_validate = array(
		array('count','checkgcount','登记货物数量必须大于0！',1,'callback'),
	);
	
	protected function getdoer(){
		return session('mg_name');
	}

	protected function checkgcount($count){
		if($count > 0){
			return true;
		}else{
			return false;
		}
	}
}