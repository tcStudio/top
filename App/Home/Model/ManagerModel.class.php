<?php
namespace Home\Model;
use Think\Model;
class ManagerModel extends Model{
    protected $_validate = array(
		array('mg_name','require','用户名不能为空！'),
		array('mg_name','','用户名称已经存在！',0,'unique',1),
		array('mg_id','require','参数传输错误！'),
		array('mg_pwd','require','密码不能为空！',0,'',1),
		array('c_mg_pwd','mg_pwd','确认密码不正确',0,'confirm',1),
	);
	
	public function add_mg(){
		$data['mg_name'] = I('post.mg_name','','trim');
		$data['mg_pwd'] = I('post.mg_pwd','','md5');
		$data['mg_last_time'] = time();
		$data['mg_add_time'] = time();
		$data['mg_role_id'] = I('post.mg_role_id','','trim');
		$data['mg_bm_id'] = I('post.mg_bm_id','','trim');
		$data['mg_c_id'] = I('post.mg_c_id','','trim');
		return $this->add($data);
	}
	
	public function update_mg(){
		$data['mg_id'] = I('post.mg_id','','trim');
		$data['mg_pwd'] = I('post.mg_pwd','','md5');
		$data['mg_role_id'] = I('post.mg_role_id','','trim');
		$data['mg_bm_id'] = I('post.mg_bm_id','','trim');
		$data['mg_c_id'] = I('post.mg_c_id','','trim');
		return $this->save($data);
	}
	
	
}