<?php
namespace Home\Model;
use Think\Model;
class GoodsLogModel extends Model{
	protected $tableName = 'goods_log';
	protected $_auto = array (
		array('doer','getdoer',1,'callback'),
		array('checker','getdoer',2,'callback'),
		array('ischeck','0'),
		array('d_time','time',1,'function'),
		array('c_time','time',2,'function'), 
	);

    protected $_validate = array(
		array('gcount','checkgcount','登记货物数量必须大于0！',1,'callback'),
		array('aid','require','参数传输错误！'),
		array('action','require','入库原因不能为空！'),
	);
	
	protected function getdoer(){
		return session('mg_name');
	}

	protected function checkgcount($gcount){
		if($gcount > 0){
			return true;
		}else{
			return false;
		}
	}
}