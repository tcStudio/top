<?php
namespace Home\Model;
use Think\Model;
class GoodsModel extends Model{
	protected $_auto = array (
		array('g_fzr','getfzr',1,'callback'),
		array('g_addtime','time',1,'function'),
		array('g_updatetime','time',3,'function'), 
	);

    protected $_validate = array(
		array('g_name','require','货物名称不能为空！'),
		array('g_id','require','参数传输错误！'),
	);
	
	Public function getfzr(){
		return session('mg_name');
	}
}