<?php
namespace Home\Common ;
Class Category{
	Static Public function unlimitedForLevel($cate,$html = '&nbsp;&nbsp;', $pid = 0, $level = 0){
		$arr = array();
		foreach($cate as $v){
			if($v['pid'] == $pid){
				$v['level'] = $level + 1;
				$v['html'] = str_repeat($html,$level);
				$arr[] = $v ;
				$arr = array_merge($arr, self::unlimitedForLevel($cate, $html,$v['id'],$level +1 ));
			}
		}
		return $arr ;
	}
	
	Static Public function unlimitedForLayer($cate, $pid = 0){
		$arr = array();
		foreach($cate as $v){
			if($v['pid'] == $pid){
				$v['b_child'] = self::unlimitedForLayer($cate, $v['id']);
				$arr[] = $v;
			}
		}
		return $arr ;
	}
	
	Static Public function getParents($cate,$id){
		$arr = array();
		foreach($cate as $v){
			if($v['id'] == $id){
				$arr[] = $v ;
				$arr = array_merge(self::getParents($cate,$v['pid']) ,$arr);
			}
		}
		return $arr;
	}
	
	Static Public function getChildsId($cate, $pid){
		$arr = array();
		foreach($cate as $v){
			if($v['pid'] == $pid){
				$arr[] = $v['id'];
				$arr = array_merge($arr, self::getChildsId($cate,$v['id']));
			}
		}
		return $arr;
	}
	//获取当前文件夹下的文件夹，网盘专用
	Static Public function getChilds($cate, $pid = 0){
		$arr = array();
		foreach($cate as $v){
			if($v['pid'] == $pid){
				$arr[] = $v;
			}
		}
		return $arr;
	}
}