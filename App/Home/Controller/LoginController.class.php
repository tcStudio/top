<?php
namespace Home\Controller;
use Think\Controller;

class LoginController extends Controller{
    public function index(){
		
		if (isset($_SESSION['mg_id'])){
			$this->redirect('Index/index');
		}
		
        if(IS_POST){
            $data['mg_name']=I('post.admin_user');
            $data['mg_pwd']=I('post.admin_psd','','md5');
			$db = M('manager');
            $row = $db->where($data)->find();
            if($row){
                session('mg_id',$row['mg_id']);
				session('mg_name',$row['mg_name']);
				session('mg_role_id',$row['mg_role_id']);
                session('mg_bm_id',$row['mg_bm_id']);
                session('mg_c_id',$row['mg_c_id']);
				//更新最后登录时间与IP
				$data =array(
					'mg_id' => $row['mg_id'],
					'mg_last_time' => time(),
					);
				$db->save($data);
                $this->redirect('Index/index');
            }else {
                $this->error('用户名或密码错误', U('index'), 4);
            }           
        }
        $this->display();
    }
	
	Public function logout(){
		session_unset();
		session_destroy();
		$this->redirect('/Login/index');
	}
	
    public function verifyImg(){
        $config=array(
            'imageW'    =>  120,
            'imageH'    =>40,
            'fontSize'  =>15,
            'length'    =>4,
            'fontttf'   =>'4.ttf'
        );
        $obj=new \Think\Verify($config);
        $obj->entry();
    }
}