<?php
namespace Home\Controller;
class GoodsController extends AdminController{
	public function showlist(){
		$where = array();
		$dw_id = I('get.dw_id',0,'intval');
		$g_name = I('get.g_name','','trim');
		if($dw_id){
			$where['g_dw_id'] = $dw_id;
		}
		if($g_name){
			$where['g_name'] = array('LIKE',"%$g_name%") ;
		}
		$db = M('Goods');
		$count = $db->where($where)->count();
		$page = new \Think\Page($count,20);
		$page->rollPage=6;
		$page->lastSuffix =false;
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$page->setConfig('first', '首页');
		$page->setConfig('last', '末页');
		$page->setConfig('theme', '共 %TOTAL_ROW% 条记录,当前是 %NOW_PAGE%/%TOTAL_PAGE% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
		$show = $page->show();
		$g_list = $db->join('LEFT JOIN top_goods_dw ON top_goods_dw.dw_id = top_goods.g_dw_id')
					->where($where)->order('g_num desc,g_id')->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('page',$show);
		$this->assign('g_list',$g_list);
		$this->display();
	}
	public function add(){
		if(IS_POST){
			$g_db = D("Goods"); // 实例化User对象
			if (!$g_db->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
				$this->error ($g_db->getError());
			}else{
				if($g_db->add()){
					 $this->success ('添加成功',U('showlist'),1);
				}else{
					$this->error('添加失败');
				}
			}
		}else{
			$dw = M('Goods_dw')->select();
			$this->assign('dw',$dw);
			$this->display();
		}
		
	}
	public function update(){
		$id = I('g_id',0,'intval');
		if($id != 0){
			if(IS_POST){
				$db = D("Goods"); // 实例化User对象
				if (!$db->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
					$this->error ($db->getError());
				}else{
					if($db->save()){
						 $this->success ('修改成功',U('showlist'),1);
					}else{
						$this->error ('修改失败');
					}
				}
			}else{
				$dw = M('Goods_dw')->select();
				$this->assign('dw',$dw);
				$db = D("Goods");
				$g_info = $db->find($id);
				$this->assign('info',$g_info);
				$this->display();
			}
		}else{
			$this->error('参数传输错误');
		}
	}
	public function delete(){
		$id = I('g_id',0,'intval');
		if($id == 0){
			$this->error('参数传输错误');
		}else{
			$g_info = M('Goods_count')->where("gid = $id")->find();
			if($g_info['gcount'] > 0){
				$this->error('该货物还有库存不能删除！');
			}else{				
				if(M("Goods")->delete($id)){
					M("goods_log")->where(array('gid' => $id))->delete();
					M("goods_count")->delete($g_info['cid']);
					$this->success('删除成功',U('showlist'));
				}else{
					$this->error('删除失败，'.M("Goods")->getError());
				}
			}
			
		}
	}

	public function dwlist(){
		$db = M('Goods_dw');
		$dwlist = $db->select();
		$this->assign('dwlist',$dwlist);
		$this->display();
	}

	public function dwadd(){
		if(IS_POST){
			$dw = D("GoodsDw"); // 实例化User对象
			if (!$dw->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
				$this->error ($dw->getError());
			}else{
				if($dw->add()){
					 $this->success ('添加成功',U('dwlist'),1);
				}else{
					$this->error ('添加失败');
				}
			}
		}else{
			$this->display();
		}
	}

	public function update_dw(){
		$id = I('dw_id',0,'intval');
		if($id != 0){
			if(IS_POST){
				$dw = D("GoodsDw"); // 实例化User对象
				if (!$dw->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
					$this->error ($dw->getError());
				}else{
					if($dw->save()){
						 $this->success ('添加成功',U('dwlist'),1);
					}else{
						$this->error ('添加失败');
					}
				}
			}else{
				$db = M('Goods_dw');
				$dwinfo = $db->find($id);
				$this->assign('dwinfo',$dwinfo);
				$this->display();
			}
		}else{
			$this->error('参数传输错误');
		}
	}

	public function del_dw(){
		$id = I('get.dw_id',0,'intval');
		if($id == 0){
			$this->error('参数传输错误');
		}else{
			if(M("Goods_dw")->delete($id)){
				$this->success('删除成功',U('showlist'));
			}else{
				$this->error('删除失败，'.M("Goods_dw")->getError());
			}
		}
	}
}