<?php
namespace Home\Controller;
class WactionController extends AdminController{

	Public function showlist(){
		$aid = I('aid',0,'intval');
		if($aid){
			$where['a_mark'] = $aid;
		}
		$db = M('w_action');
		$count = $db->where($where)->count();
		$page = new \Think\Page($count,20);
		$page->rollPage=6;
		$page->lastSuffix =false;
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$page->setConfig('first', '首页');
		$page->setConfig('last', '末页');
		$page->setConfig('theme', '共 %TOTAL_ROW% 条记录,当前是 %NOW_PAGE%/%TOTAL_PAGE% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
		$show = $page->show();
		$alist = $db->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('alist',$alist);
		$this->display();
	}

	Public function add(){
		if(IS_POST){
			$data['a_mark'] = I('a_mark','0','intval');
			$data['a_info'] = I('a_info','','trim');
			if($data['a_info']){
				if(M('w_action')->add($data)){
					$this->success('添加成功',U('showlist'));
				}else{
					$this->error('添加失败');
				}
			}
		}else{
			$this->display();
		}
	}

	Public function update(){
		$id = I('id',0,'intval');
		if($id == 0){
			$this->error('参数错误');
		}
		$db = M('w_action');
		if(IS_POST){
			$data['a_id'] = $id;
			$data['a_mark'] = I('a_mark','0','intval');
			$data['a_info'] = I('a_info','','trim');
			if($data['a_info']){
				if($db->save($data)){
					$this->success('修改成功',U('showlist'));
				}else{
					$this->error('修改失败');
				}
			}
		}else{
			$action = $db->find($id);
			$this->assign('action',$action);
			$this->display();
		}
	}

	Public function del(){
		$id = I('id',0,'intval');
		if($id == 0){
			$this->error('参数错误');
		}else{
			$db = M('w_action');
			if($db->delete($id)){
				$this->success('删除成功',U('showlist'));
			}else{
				$this->error('删除失败');
			}
		}		
	}
}