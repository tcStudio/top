<?php
namespace Home\Controller;
class IndexController extends AdminController{
    public function index(){
        $this->display();
    }
    public function head(){
        $this->display();
    }
    public function left(){
        $manager=M('Manager')->find(session('mg_id'));  //获得当前的管理员
        $role=M('role')->find($manager['mg_role_id']);  //通过角色的id获取角色的信息
        $auth_ids=$role['role_auth_ids'];               //角色的权限的集合
        if($manager['mg_role_id']==1)
        {
            $info1=M('auth')->where("auth_level=0 and auth_menu=1")->order('auth_sort desc')->select();
            $info2=M('auth')->where("auth_level=1 and auth_menu=1")->order('auth_sort desc')->select();
        }else{
            $info1=M('auth')->where("auth_level=0 and auth_menu=1 and auth_id in ($auth_ids)")->order('auth_sort desc')->select();
            $info2=M('auth')->where("auth_level=1 and auth_menu=1 and auth_id in ($auth_ids)")->order('auth_sort desc')->select();
        }
        $this->assign('info1',$info1);
        $this->assign('info2',$info2);
        $this->display();
    }
    public function right(){
        $user = M('Manager')->join('LEFT JOIN top_role ON top_role.role_id = top_manager.mg_role_id')->find(session('mg_id'));
        $ip = get_client_ip();
        $this->assign('user',$user);
        $this->assign('ip',$ip);
        $this->display();
    }
}