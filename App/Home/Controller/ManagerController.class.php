<?php
namespace Home\Controller;
class ManagerController extends AdminController{
	public function showlist(){
		$db = M('Manager');
		$count = $db->where('mg_role_id > 1')->count();
		$page = new \Think\Page($count,20);
		$page->rollPage=6;
		$page->lastSuffix =false;
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$page->setConfig('first', '首页');
		$page->setConfig('last', '末页');
		$page->setConfig('theme', '共 %TOTAL_ROW% 条记录,当前是 %NOW_PAGE%/%TOTAL_PAGE% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
		$show = $page->show();
		$fields = array(
			'top_manager.mg_id',
			'top_manager.mg_name',
			'top_manager.mg_last_time',
			'top_role.role_name',
			'top_bumen.name' => 'b_name',
			'top_goods_cate.name' => 'c_name',
			);
		$mg_list = $db->field($fields)->join('LEFT JOIN top_role ON top_role.role_id = top_manager.mg_role_id')
					->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_manager.mg_c_id')
					->join('LEFT JOIN top_bumen ON top_bumen.id = top_manager.mg_bm_id')
					->where('mg_role_id > 1')->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('page',$show);
		$this->assign('mg_list',$mg_list);	
		$this->display();
	}
	public function add(){
		if(IS_POST){
			$mg = D("manager"); // 实例化User对象
			if (!$mg->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
				$this->error ($mg->getError());
			}else{
				if($mg->add_mg()){
					 $this->success ('添加成功',U('showlist'),1);
				}else{
					$this->error ('添加失败');
				}
			}
		}else{
			$bm_db = M('Bumen');
			$bm_list = $bm_db->select();
			$cate = new \Home\Common\Category;
			$bm = $cate->unlimitedForLevel($bm_list);
			$this->assign('bm',$bm);
			$c_db = M('Goods_cate');
			$c_list = $c_db->select();
			$c_list = $cate->unlimitedForLevel($c_list);
			$this->assign('c_list',$c_list);
			$role_l = M('role')->order('role_sort desc')->select();
			$this->assign('role_l',$role_l);
			$this->display();
		}
		
	}
	public function update(){
		$id = I('mg_id',0,'intval');
		if($id != 0){
			if($id == session('mg_id') || session('mg_role_id') == 1){
				if(IS_POST){
					$mg = D("manager"); // 实例化User对象
					if (!$mg->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
						$this->error ($mg->getError());
					}else{
						if($mg->update_mg()){
							 $this->success ('修改成功',U('showlist'),1);
						}else{
							$this->error ('修改失败');
						}
					}
				}else{
					$bm_db = M('Bumen');		
					$bm_list = $bm_db->select();
					$cate = new \Home\Common\Category;
					$bm = $cate->unlimitedForLevel($bm_list);
					$this->assign('bm',$bm);
					$c_db = M('Goods_cate');
					$c_list = $c_db->select();
					$c_list = $cate->unlimitedForLevel($c_list);
					$this->assign('c_list',$c_list);
					$mg = M('Manager')->join('top_role ON top_role.role_id = top_manager.mg_role_id')->find($id);
					$role_l = M('role')->order('role_sort desc')->select();
					$this->assign('role_l',$role_l);
					$this->assign('mg',$mg);
					$this->display();
				}
			}else{
				$this->error('你没有修改别人信息的权限！');
			}
		}else{
			$this->error('参数传输错误');
		}
	}

	public function password(){
		if(IS_POST){
			$oldpw = I('old_pwd','','trim');
			$mg_pwd = I('mg_pwd','','trim');
			$c_mg_pwd = I('c_mg_pwd','','trim');
			if($oldpw == ''){
				$this->error('旧密码不能为空');
			}
			if($mg_pwd == ''){
				$this->error('新密码不能为空');
			}
			if($mg_pwd != $c_mg_pwd){
				$this->error('密码不一致');
			}
			$db = M('manager');
			$id = session('mg_id');
			$oldpw = md5($oldpw);
			$c_oldpw = $db->where(array('mg_id' =>$id))->getField('mg_pwd');
			if($oldpw != $c_oldpw){
				$this->error('旧密码不正确');
			}else{
				if($db->where(array('mg_id' =>$id))->setField('mg_pwd',md5($mg_pwd))){
					$this->success('密码修改成功');
				}else{
					$this->error('密码修改失败');
				}
			}
			
		}else{
			$this->display();
		}
	}

	public function delete(){
		$id = I('get.mg_id',0,'intval');
		if($id == 0){
			$this->error('参数传输错误');
		}else{
			if(M("manager")->delete($id)){
				$this->success('删除成功',U('showlist'));
			}else{
				$this->error('删除失败，'.M("manager")->getError());
			}
		}
	}
}