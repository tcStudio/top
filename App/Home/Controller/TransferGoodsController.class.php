<?php
namespace Home\Controller;
class TransferGoodsController extends AdminController{
	Public function showlist(){
		$ischeck = I('ischeck',0,'intval');
		$db = M('transfer_log');
		$count = $db->where(array('ischeck'=>$ischeck))->count();
		$page = new \Think\Page($count,20);
		$page->rollPage=6;
		$page->lastSuffix =false;
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$page->setConfig('first', '首页');
		$page->setConfig('last', '末页');
		$page->setConfig('theme', '共 %TOTAL_ROW% 条记录,当前是 %NOW_PAGE%/%TOTAL_PAGE% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
		$show = $page->show();
		$fields = array(
			'a.id',
			'a.gid',
			'b.g_name',
			'c.name'  => 'from',
			'd.name'  => 'to',
			'a.count',
			'e.dw_name',
			'a.ischeck',
			'a.doer',
			'a.d_time'
			);
		$log_l = $db->where(array('ischeck'=>$ischeck))->field($fields)->alias('a')
					->join('top_goods b ON b.g_id = a.gid')
					->join('top_goods_cate c ON c.id = a.from_w')
					->join('top_goods_cate d ON d.id = a.to_w')
					->join('top_goods_dw e ON e.dw_id = b.g_dw_id')
					->order('a.id desc')->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('page',$show);
		$this->assign('log_l',$log_l);
		$this->display();
	}

	Public function transfer(){	
		if(IS_POST){
			$db =D('TransferLog');
			if($db->create()){
				if($db->add()){
					$this->success ('调拨登记成功',U('showlist'));
				}else{
					$this->error ('调拨登记失败');
				}
			}else{
				$this->error ($db->getError());
			}
		}else{
			$cid = I('cid',0,'intval');
			if($cid == 0){
				$this->error('参数传输错误');
			}
			$c_db = M('Goods_cate');
			$c_list = $c_db->select();
			$cate = new \Home\Common\Category;
			$c_list = $cate->unlimitedForLevel($c_list);
			$this->assign('c_list',$c_list);
			$db = M('goods_count') ;
			$g_info = $db->join('LEFT JOIN top_goods ON top_goods_count.gid = top_goods.g_id')
				->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_goods_count.bid')
				->join('LEFT JOIN top_goods_dw ON top_goods_dw.dw_id = top_goods.g_dw_id')->find($cid);
			$this->assign('info',$g_info);
			$this->display();
		}
	}

	Public function check(){
		$id = I('id',0,'intval');
		$mark = I('get.mark',0,'intval');
		if($id <= 0){
			$this->error("参数传送错误");
		}
		if($mark != 1 && $mark != 2){
			$this->error("操作参数不存在");
		}
		$db = D('TransferLog');
		$info = $db->find($id);
		if($info && $info['ischeck'] == '0'){
			if($mark == 1){
				$c_db = M('goods_count');
				$f_info = $c_db->where(array('gid'=>$info['gid'],'bid'=>$info['from_w']))->find();
				if($f_info){
					if($f_info['gcount'] >= $info['count']){
						$f_info['gcount'] = $f_info['gcount'] - $info['count'] ;
						$re = $c_db->save($f_info);
						$t_info = $c_db->where(array('gid'=>$info['gid'],'bid'=>$info['to_w']))->find();
						if($t_info){
							$t_info['gcount'] = $t_info['gcount'] + $info['count'];
							$re = $c_db->save($t_info);
						}else{
							$t_info['bid'] = $info['to_w'];
							$t_info['gid'] = $info['gid'];
							$t_info['gcount'] = $info['count'];
							$re = $c_db->add($t_info);
						}
						if($re){
							$info['ischeck'] = 1 ;
							$db->create($info);
							$db->save();
							$l_db = M('goods_log');
							$f_log = array(
								'gid' => $info['gid'],
								'aid' => 2,
								'bid' => $info['from_w'],
								'gcount' => $info['count'],
								'action' => 1,
								'doer' => $info['doer'],
								'ischeck' => 1,
								'checker' => session('mg_name'),
								'd_time' => $info['d_time'],
								'c_time' => time()
								);
							$t_log = array(
								'gid' => $info['gid'],
								'aid' => 1,
								'bid' => $info['to_w'],
								'gcount' => $info['count'],
								'action' => 1,
								'doer' => $info['doer'],
								'ischeck' => 1,
								'checker' => session('mg_name'),
								'd_time' => $info['d_time'],
								'c_time' => time()
								);
							$l_db->add($f_log);
							$l_db->add($t_log);
							$this->success('审核成功',U('showlist'));
						}else{
							$this->error('系统故障！');
						}
					}else{
						$this->error('你要调出的货物库存不足');
					}
				}else{
					$this->error('你要调出的货物在仓库中找不到');
				}
			}else{
				$info['ischeck'] = 2 ;
				if($db->create($info)){
					if($db->save()){
						$this->success("拒绝申请成功",U('showlist'));
					}
				}else{
					$this->error("拒绝申请失败".$db->getError());
				}
			}
		}else{

			$this->error("申请记录异常！");
		}
	}

	Public function delete(){
		$id = I('id',0,'intval');
		if($id > 0){
			if(M('transfer_log')->delete($id)){
				$this->success('删除成功',U('showlist'));
			}else{
				$this->error("删除失败");
			}
		}else{
			$this->error("参数传送错误");
		}
	}
}