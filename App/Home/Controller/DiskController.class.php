<?php
namespace Home\Controller;

class DiskController extends AdminController{
	Public function index(){
		$d_db = M('Disk');
		$f_db = M('Files');
		$did = I('did',0,'intval');
		$uid = session('mg_id');
		$folders = $d_db->where(array('uid' => $uid))->select();
		$files = $f_db->where(array('uid' => $uid,'did' =>$did))->select();
		$cate = new \Home\Common\Category;
		$current_folder = '' ;
		$up_folder = '' ;
		$current_path ='';
		if($folders){
			$current_folder = $cate->getChilds($folders,$did);
			$up_folder = $cate->getParents($folders,$did);
		}
		if($did !== 0){
			$current_path = $d_db->find($did);
		}
		$this->assign('info',$current_path);
		$this->assign('loc',$up_folder);
		$this->assign('files',$files);
		$this->assign('f_list',$current_folder);
		$this->display();
	}

	Public function slist(){
		$u_db = M('Manager');
		$f_db = M('Files');
		$uid = I('uid',0,'intval');
		if($uid > 0){
			$files = $f_db->where(array('uid' => $uid,'isshare' =>1))->select();
			$uinfo = $u_db->find($uid);
			$this->assign('files',$files);
			$this->assign('uinfo',$uinfo);
		}else{
			$fields = array(
			'top_manager.mg_id',
			'top_manager.mg_name',
			);
			$user = $u_db->distinct(true)->field($fields)->join('LEFT JOIN top_files ON top_manager.mg_id = top_files.uid')
			->where('top_files.isshare = 1')->select();
			$this->assign('u_list',$user);
		}
		$this->display();
	}

	Public function mkdir(){
		$pid = I('post.pid',0,'intval');
		$filename = I('post.filename','','trim');
		if($filename === ''){
			$this->error('文件夹名不能为空');
		}else{
			$data = array(
				'pid' => $pid,
				'name' => $filename,
				'uid' => session('mg_id'),
				);
			if(M('Disk')->add($data)){
				$this->success('新建文件夹成功',U('index',array('did' => $pid)));
			}else{
				$this->error('新建文件夹失败');
			}
		}
	}

	Public function sharef(){
		$data['fid'] = I('fid',0,'intval');
        $data['isshare'] = I('share',0,'intval');
		if($data['fid'] > 0){
			if(M('Files')->save($data)){
				$result['status'] = 1;
				$result['message'] = "共享状态更新成功";
				$this->ajaxReturn($result);
			}else{
				$result['status'] = 0;
				$result['message'] = "共享状态更新失败";
				$this->ajaxReturn($result);
			}
		}
	}

	Public function download(){
		$fid = I('fid',0,'intval');
		if($fid > 0){
			$info = M('Files')->find($fid);
			if(file_exists($info['path'])){
				$info['fname'] = iconv("utf-8","gb2312",$info['fname']);
				set_time_limit(0);
				ini_set('memory_limit', '512M');
				header('Content-Type: application/octet-stream');
				header("Content-Disposition:attachment;filename=".$info['fname']);
				header('Content-Transfer-Encoding: binary');
				ob_end_clean();
				readfile($info['path']);
			}else{
				$this->error('你要下载的文件丢失了');
			}
			
		}else{
			$this->error('参数传输错误');
		}
	}

	Public function delf(){
		$fid = I('fid',0,'intval');
		if($fid > 0){
			$info = M('Files')->find($fid);
			if(file_exists($info['path'])){
				unlink($info['path']);
			}
			if(M('Files')->delete($fid)){
				$result['status'] = 1;
				$result['message'] = "删除成功";
				$this->ajaxReturn($result);
			}			
		}else{
			$result['status'] = 0;
			$result['message'] = "删除失败";
			$this->ajaxReturn($result);
		}
	}

	Public function deld(){
		$d_db = M('Disk');
		$f_db = M('Files');
		$did = I('did',0,'intval');
		$uid = session('mg_id');
		$folders = $d_db->where(array('uid' => $uid))->select();
		if($did > 0){
			$cate = new \Home\Common\Category;
			$d_folder = $cate->getChildsId($folders,$did);
			$d_folder[] = $did;
			$where['did'] = array('IN' , $d_folder);
			$d_files = $f_db->where($where)->select();
			if($d_files){
				foreach($d_files as $v){
					unlink($v['path']);
					$f_db->delete($v['fid']);
				}
			}
			
			if($d_db->where(array('id' =>array('IN' , $d_folder)))->delete()){
				$result['status'] = 1;
				$result['message'] = "删除成功";
				$this->ajaxReturn($result);
			}
		}else{
			$result['status'] = 0;
			$result['message'] = "参数传输错误";
			$this->ajaxReturn($result);
		}
	}

	Public function upload(){
		if (!empty($_FILES)) {
			//图片上传设置
			$upload = new \Think\Upload();// 实例化上传类
			$images = $upload->upload();
			//判断是否有文件
			if($images){
				$data = array(
					'did' => I('did',0,'intval'),
					'uid' => I('uid',0,'intval'),
					'fname' => $images['Filedata']['name'],
					'path' => "./Uploads/" . $images['Filedata']['savepath'] . $images['Filedata']['savename'],
					'filetype' => $images['Filedata']['ext'],
					'filesize' => $images['Filedata']['size'],
					'addtime' => time()
					);
				$info = M('Files')->add($data);
				echo 1;
			}else{
				$this->error($upload->getError());//获取失败信息
			}
		}
	}
}