<?php
namespace Home\Controller;
class WareroomController extends AdminController{
	//入库货物查询
	Public function add_to_w(){

		$where = array();
		$g_name = I('get.g_name','','trim');

		if($g_name){
			$where['g_name'] = array('LIKE',"%$g_name%") ;
		}
		$db = M('Goods');
		$count = $db->where($where)->count();
		$page = new \Think\Page($count,20);
		$page->rollPage=6;
		$page->lastSuffix =false;
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$page->setConfig('first', '首页');
		$page->setConfig('last', '末页');
		$page->setConfig('theme', '共 %TOTAL_ROW% 条记录,当前是 %NOW_PAGE%/%TOTAL_PAGE% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
		$show = $page->show();
		$g_list = $db->join('LEFT JOIN top_goods_dw ON top_goods_dw.dw_id = top_goods.g_dw_id')
					->join('LEFT JOIN top_goods_count ON top_goods_count.gid = top_goods.g_id')
					->where($where)->order('g_num desc,g_id')->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('page',$show);
		$this->assign('g_list',$g_list);
		$this->display();
	}
	//入库申请登记
	Public function add(){
		$g_id = I('gid',0,'intval');
		if($g_id){
			if(IS_POST){
				$db =D('GoodsLog');
				if (!$db->create()){
					$this->error ($db->getError());
				}else{
					if($db->add()){
						 $this->success ('入库登记成功',U('check_l'));
					}else{
						$this->error ('入库登记失败');
					}
				}
			}else{
				$c_db = M('Goods_cate');
				$c_list = $c_db->select();
				$cate = new \Home\Common\Category;
				$c_list = $cate->unlimitedForLevel($c_list);
				$this->assign('c_list',$c_list);
				$db = M('Goods') ;
				$g_info = $db->join('LEFT JOIN top_goods_dw ON top_goods_dw.dw_id = top_goods.g_dw_id')->find($g_id);
				$action = M('w_action')->where(array('a_mark' =>1))->select();
				$this->assign('action',$action);
				$this->assign('info',$g_info);
				$this->display();
			}
		}else{
			$this->error('参数传送错误');
		}
	}
	//出入库待审列表
	Public function check_l(){
		$c_db = M('Goods_cate');
		$c_list = $c_db->select();
    	$cate = new \Home\Common\Category;
    	$b_id = session('mg_c_id');
    	if($b_id){
			$b_ids = $cate->getChildsId($c_list,$b_id);			
			if($b_ids){
				$b_ids[] = $b_id ;
				$where['top_goods_log.bid'] = array('IN',$b_ids) ;
			}else{
				$where['top_goods_log.bid'] = $b_id;
			}
		}
		$where['top_goods_log.ischeck'] = '0';
    	$db = M('Goods_log');
		$count = $db->where($where)->count();
		$page = new \Think\Page($count,20);
		$page->rollPage=6;
		$page->lastSuffix =false;
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$page->setConfig('first', '首页');
		$page->setConfig('last', '末页');
		$page->setConfig('theme', '共 %TOTAL_ROW% 条记录,当前是 %NOW_PAGE%/%TOTAL_PAGE% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
		$show = $page->show();
		$log_l = $db->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_goods_log.bid')
					->join('LEFT JOIN top_goods ON top_goods.g_id = top_goods_log.gid')
					->join('LEFT JOIN top_goods_dw ON top_goods.g_dw_id = top_goods_dw.dw_id')
					->join('LEFT JOIN top_w_action ON top_w_action.a_id = top_goods_log.action')
					->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('page',$show);
		$this->assign('log_l',$log_l);
		$this->display();
	}
	//审核出入库请求
	Public function check(){
		$lid = I('lid',0,'intval');
		if($lid <= 0){
			$this->error("参数传送错误");
		}
		$l_db = D('GoodsLog');
		$l_info = $l_db->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_goods_log.bid')
					   ->join('LEFT JOIN top_goods ON top_goods.g_id = top_goods_log.gid')
					   ->join('LEFT JOIN top_goods_dw ON top_goods.g_dw_id = top_goods_dw.dw_id')
					   ->join('LEFT JOIN top_w_action ON top_w_action.a_id = top_goods_log.action')->find($lid);
		$this->assign('l_info',$l_info);
		$this->display();		
	}
	//处理出入库请求
	Public function check_d(){
		$lid = I('lid',0,'intval');
		$cnum = I('get.cnum',0,'intval');
		if($lid <= 0){
			$this->error("参数传送错误");
		}
		if($cnum != 1 && $cnum != 2){
			$this->error("操作参数不存在");
		}
		$l_db = D('GoodsLog');
		$l_info = $l_db->find($lid);
		if($l_info && $l_info['ischeck'] == '0'){
			if($cnum == 1){
				$data['gid'] = $l_info['gid'] ;
				$data['bid'] = $l_info['bid'] ;
				$data['gcount'] = $l_info['gcount'] ;
				$c_db = M('Goods_count');
				$c_info = $c_db->where(array('gid' => $l_info['gid'] ,'bid' => $l_info['bid'] ))->find();
				if($c_info){
					if($l_info['aid'] == 1){
						$c_info['gcount'] = $c_info['gcount'] + $l_info['gcount'];
						$re = $c_db->save($c_info);
					}else{
						if($c_info['gcount'] < $l_info['gcount']){
							$this->error("库存不足，不能调出！");
						}else{
							$c_info['gcount'] = $c_info['gcount'] - $l_info['gcount'];
							$re = $c_db->save($c_info);
						}
					}	
				}else{
					$re = $c_db->add($data);
				}
				if($re){
					$l_info['ischeck'] = 1 ;
					if($l_db->create($l_info)){
						if($l_db->save()){
							$this->success("批准申请成功",U('check_l'));
						}
					}
					
				}else{
					$this->error("批准申请失败".$c_db->getError());
				}
			}else{
				$l_info['ischeck'] = 2 ;
				if($l_db->create($l_info)){
					if($l_db->save()){
						$this->success("拒绝申请成功",U('check_l'));
					}
				}else{
					$this->error("拒绝申请失败".$l_db->getError());
				}
			}
		}else{
			
			$this->error("申请记录异常！");
		}
	}

	//库存列表
	Public function wlist(){
		$c_db = M('Goods_cate');
		$c_list = $c_db->select();
    	$cate = new \Home\Common\Category;
    	$c_list = $cate->unlimitedForLevel($c_list);
		$this->assign('c_list',$c_list);

		$where = array();

		$b_id = I('get.b_id',0,'intval');
		$g_name = I('get.g_name','','trim');

		if($b_id){
			$b_ids = $cate->getChildsId($c_list,$b_id);			
			if($b_ids){
				$b_ids[] = $b_id ;
				$where['top_goods_count.bid'] = array('IN',$b_ids) ;
			}else{
				$where['top_goods_count.bid'] = $b_id;
			}
		}
		if($g_name){
			$where['top_goods.g_name'] = array('LIKE',"%$g_name%") ;
		}
		$db = M('Goods_count');
		$count = $db->join('LEFT JOIN top_goods ON top_goods_count.gid = top_goods.g_id')
					->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_goods_count.bid')
					->join('LEFT JOIN top_goods_dw ON top_goods_dw.dw_id = top_goods.g_dw_id')
					->where($where)->count();
		$page = new \Think\Page($count,20);
		$page->rollPage=6;
		$page->lastSuffix =false;
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$page->setConfig('first', '首页');
		$page->setConfig('last', '末页');
		$page->setConfig('theme', '共 %TOTAL_ROW% 条记录,当前是 %NOW_PAGE%/%TOTAL_PAGE% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
		$show = $page->show();
		$g_list = $db->join('LEFT JOIN top_goods ON top_goods_count.gid = top_goods.g_id')
					->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_goods_count.bid')
					->join('LEFT JOIN top_goods_dw ON top_goods_dw.dw_id = top_goods.g_dw_id')
					->where($where)->order('g_num desc,g_id')->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('page',$show);
		$this->assign('g_list',$g_list);
		$this->display();
	}

	//库存预警列表
	Public function yjlist(){
		$c_db = M('Goods_cate');
		$c_list = $c_db->select();
    	$cate = new \Home\Common\Category;
    	$c_list = $cate->unlimitedForLevel($c_list);
		$this->assign('c_list',$c_list);

		$where = array();

		$b_id = I('get.b_id',0,'intval');
		$g_name = I('get.g_name','','trim');

		if($b_id){
			$b_ids = $cate->getChildsId($c_list,$b_id);			
			if($b_ids){
				$b_ids[] = $b_id ;
				$where['top_goods_count.bid'] = array('IN',$b_ids) ;
			}else{
				$where['top_goods_count.bid'] = $b_id;
			}
		}
		if($g_name){
			$where['top_goods.g_name'] = array('LIKE',"%$g_name%") ;
		}
		$db = M('Goods_count');
		$count = $db->join('LEFT JOIN top_goods ON top_goods_count.gid = top_goods.g_id')
					->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_goods_count.bid')
					->join('LEFT JOIN top_goods_dw ON top_goods_dw.dw_id = top_goods.g_dw_id')
					->where($where)->where('top_goods.g_yjs > 0 AND top_goods.g_yjs > top_goods_count.gcount')->count();
		$page = new \Think\Page($count,20);
		$page->rollPage=6;
		$page->lastSuffix =false;
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$page->setConfig('first', '首页');
		$page->setConfig('last', '末页');
		$page->setConfig('theme', '共 %TOTAL_ROW% 条记录,当前是 %NOW_PAGE%/%TOTAL_PAGE% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
		$show = $page->show();
		$g_list = $db->join('LEFT JOIN top_goods ON top_goods_count.gid = top_goods.g_id')
					->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_goods_count.bid')
					->join('LEFT JOIN top_goods_dw ON top_goods_dw.dw_id = top_goods.g_dw_id')
					->where($where)->where('top_goods.g_yjs > 0 AND top_goods.g_yjs > top_goods_count.gcount')
					->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('page',$show);
		$this->assign('g_list',$g_list);
		$this->display();
	}

	//出库申请登记
	Public function out_w(){
		$cid = I('cid',0,'intval');
		if($cid){
			if(IS_POST){
				$db =D('GoodsLog');
				if (!$db->create()){
					$this->error ($db->getError());
				}else{
					if($db->add()){
						 $this->success ('入库登记成功',U('check_l'));
					}else{
						$this->error ('入库登记失败');
					}
				}
			}else{
				$db = M('goods_count') ;
				$g_info = $db->join('LEFT JOIN top_goods ON top_goods_count.gid = top_goods.g_id')
				->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_goods_count.bid')
				->join('LEFT JOIN top_goods_dw ON top_goods_dw.dw_id = top_goods.g_dw_id')->find($cid);
				$action = M('w_action')->where(array('a_mark' =>2))->select();
				$this->assign('action',$action);
				$this->assign('info',$g_info);
				$this->display();
			}
		}else{
			$this->error('参数传送错误');
		}
	}

	//进出记录
	Public function wlog(){
		$c_db = M('Goods_cate');
		$c_list = $c_db->select();
    	$cate = new \Home\Common\Category;
    	$c_list = $cate->unlimitedForLevel($c_list);
		$this->assign('c_list',$c_list);

    	$b_id = I('get.b_id',0,'intval');
    	$ischeck = I('get.ischeck',0,'intval');
    	$g_name = I('get.g_name','','trim');
    	if($b_id){
			$b_ids = $cate->getChildsId($c_list,$b_id);
			if($b_ids){
				$b_ids[] = $b_id ;
				$where['top_goods_log.bid'] = array('IN',$b_ids) ;
			}else{
				$where['top_goods_log.bid'] = $b_id;
			}
		}
		if($g_name){
			$where['top_goods.g_name'] = array('LIKE',"%$g_name%") ;
		}
		if($ischeck){
			$where['top_goods_log.ischeck'] = $ischeck ;
		}else{
			$where['top_goods_log.ischeck'] = array('GT',0) ;
		}
		
    	$db = M('Goods_log');
		$count = $db->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_goods_log.bid')
					->join('LEFT JOIN top_goods ON top_goods.g_id = top_goods_log.gid')
					->join('LEFT JOIN top_goods_dw ON top_goods.g_dw_id = top_goods_dw.dw_id')
					->join('LEFT JOIN top_w_action ON top_w_action.a_id = top_goods_log.action')
					->where($where)->count();
		$page = new \Think\Page($count,20);
		$page->rollPage=6;
		$page->lastSuffix =false;
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$page->setConfig('first', '首页');
		$page->setConfig('last', '末页');
		$page->setConfig('theme', '共 %TOTAL_ROW% 条记录,当前是 %NOW_PAGE%/%TOTAL_PAGE% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
		$show = $page->show();
		$log_l = $db->join('LEFT JOIN top_goods_cate ON top_goods_cate.id = top_goods_log.bid')
					->join('LEFT JOIN top_goods ON top_goods.g_id = top_goods_log.gid')
					->join('LEFT JOIN top_goods_dw ON top_goods.g_dw_id = top_goods_dw.dw_id')
					->join('LEFT JOIN top_w_action ON top_w_action.a_id = top_goods_log.action')
					->where($where)->order('lid desc')->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('page',$show);
		$this->assign('log_l',$log_l);
		$this->display();
	}
	//删除进出库记录
	public function delete(){
		$id = I('lid',0,'intval');
		if($id == 0){
			$this->error('参数传输错误');
		}else{
			if(M("Goods_log")->delete($id)){
				$this->success('删除成功',U('wlog'));
			}else{
				$this->error('删除失败，'.M("Goods")->getError());
			}
		}
	}
}