<?php
namespace Home\Controller;
class BumenController extends AdminController{
    public function showlist(){
    	$db = M('Bumen');		
		$bm_list = $db->select();
    	$cate = new \Home\Common\Category;
    	$bm = $cate->unlimitedForLevel($bm_list);
		$this->assign('bm_list',$bm);
        $this->display();
    }
    public function add(){
		if(IS_POST){
			$bm = D("Bumen"); // 实例化User对象
			if (!$bm->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
				$this->error ($bm->getError());
			}else{
				if($bm->add()){
					 $this->success ('添加成功',U('showlist'));
				}else{
					$this->error ('添加失败');
				}
			}
		}else{
			$db = M('Bumen');		
			$bm_list = $db->select();
	    	$cate = new \Home\Common\Category;
	    	$bm = $cate->unlimitedForLevel($bm_list);
			$this->assign('tbm',$bm);
			$this->display();
		}
		
    }
    public function update(){
		$id = I('id',0,'intval');
		if($id != 0){
			if(IS_POST){
				$bm = D("Bumen"); // 实例化User对象
				if (!$bm->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
					$this->error ($bm->getError());
				}else{
					if($bm->save()){
						$this->success ('修改成功',U('showlist'),1);
					}else{
						$this->error ('修改失败');
					}
				}
			}else{
				$db = M('Bumen');
				$bm_list = $db->select();
		    	$cate = new \Home\Common\Category;
		    	$bm_list = $cate->unlimitedForLevel($bm_list);
				$this->assign('bm_list',$bm_list);

				$bm_info = $db->find($id);
				$this->assign('bm_info',$bm_info);

				$this->display();
			}
			
		}else{
			$this->error('参数传输错误');
		}
         
    }
    public function delete(){
        $id = I('get.id',0,'intval');
		if($id == 0){
			$this->error('参数传输错误');
		}else{
			if(M("Bumen")->delete($id)){
				$this->success('删除成功',U('showlist'));
			}else{
				$this->error('删除失败，'.M("Bumen")->getError());
			}
		}
    }
}