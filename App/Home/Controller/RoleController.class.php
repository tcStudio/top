<?php
namespace Home\Controller;
use Think\Controller;
class RoleController extends AdminController{
    public function showlist(){
        $list=M('role')->order('role_sort desc')->select();
        $this->assign('list',$list);
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $db = D("role"); // 实例化User对象
            if (!$db->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
                $this->error ($db->getError());
            }else{
                if($db->add()){
                     $this->success ('添加成功',U('showlist'),1);
                }else{
                    $this->error ('添加失败');
                }
            }
        }else{
            $role_l = M('role')->order('role_sort desc')->select();
            $this->assign('role_l',$role_l);
            $this->display();
        }
        
    }

    public function update(){
        $id = I('role_id',0,'intval');
        if($id != 0){
            if(IS_POST){
                $db = D("role"); // 实例化User对象
                if (!$db->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
                    $this->error ($db->getError());
                }else{
                    if($db->save()){
                        $this->success ('修改成功',U('showlist'),1);
                    }else{
                        $this->error ('修改失败');
                    }
                }
            }else{
                $field = M('role')->find($id);
                $this->assign('field',$field);
                $this->display();
            }
        }else{
            $this->error('参数传输错误');
        }
    }

    //角色更改排序
    public function updateSort(){
        $data['role_id'] = I('role_id',0,'intval');
        $data['role_sort'] = I('sort',0,'intval');
        if($data['role_id'] > 0 ){
            $db = M('role');
            $old_sort = $db->where(array( 'role_id' => $data['role_id']))->getField('role_sort');
            if($old_sort != $data['role_sort']){
                if($db->save($data)){
                    $result['status'] = 1;
                    $result['message'] = "排序成功";
                    $this->ajaxReturn($result);
                }
            }
        }        
    }

    //分配权限的方法
    public function distribute($role_id){
        if($role_id==0)
            die('参数不正确');
        $role = D('Role');
        if(IS_POST){
            $model=D('Role');
            if($model->updateRole($_POST['auth'], $role_id))
                $this->success ('修改成功',U('showlist'),1);
            else
                $this->error ('修改失败');
            exit();
        }        
        $role_info=$role->find($role_id);   //角色的信息
        $role_auth_ids=$role_info['role_auth_ids']; //角色具有的权限
        $role_auth_id_array=explode(',', $role_auth_ids);   //把权限字符串切割成数组
        $this->assign('role_auth_id_array',$role_auth_id_array);
       
        $info1=M('auth')->where("auth_level=0")->select();
        $info2=M('auth')->where("auth_level=1")->select();
        $this->assign('info1',$info1);
        $this->assign('info2',$info2);
        $this->display();
    }

    public function delete(){
        $id = I('get.role_id',0,'intval');
        if($id == 0){
            $this->error('参数传输错误');
        }elseif($id == 1){
            $this->error('超级管理员不能删');
        }else{
            if(M("role")->delete($id)){
                $this->success('删除成功',U('showlist'));
            }else{
                $this->error('删除失败，'.M("role")->getError());
            }
        }
    }
}