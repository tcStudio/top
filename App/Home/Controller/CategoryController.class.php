<?php
namespace Home\Controller;
class CategoryController extends AdminController{
    public function showlist(){
    	$db = M('Goods_cate');
		$cate_list = $db->select();
    	$cate = new \Home\Common\Category;
    	$cate = $cate->unlimitedForLevel($cate_list);
		$this->assign('cate_list',$cate);
        $this->display();
    }
    public function add(){
		if(IS_POST){
			$cate = D("GoodsCate"); // 实例化User对象
			if (!$cate->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
				$this->error ($cate->getError());
			}else{
				if($cate->add()){
					 $this->success ('添加成功',U('showlist'));
				}else{
					$this->error ('添加失败');
				}
			}
		}else{
			$db = M('Goods_cate');		
			$cate_list = $db->select();
	    	$category = new \Home\Common\Category;
	    	$cate = $category->unlimitedForLevel($cate_list);
			$this->assign('tcate',$cate);
			$this->display();
		}
		
    }
    public function update(){
		$id = I('id',0,'intval');
		if($id != 0){
			if(IS_POST){
				$cate = D("GoodsCate"); // 实例化User对象
				if (!$cate->create()){     // 如果创建失败 表示验证没有通过 输出错误提示信息     
					$this->error ($cate->getError());
				}else{
					if($cate->save()){
						$this->success ('修改成功',U('showlist'),1);
					}else{
						$this->error ('修改失败');
					}
				}
			}else{
				$db = M('Goods_cate');
				$cate_list = $db->select();
		    	$category = new \Home\Common\Category;
		    	$cate_list = $category->unlimitedForLevel($cate_list);
				$this->assign('cate_list',$cate_list);

				$cate_info = $db->find($id);
				$this->assign('cate_info',$cate_info);

				$this->display();
			}
			
		}else{
			$this->error('参数传输错误');
		}
         
    }
    public function delete(){
        $id = I('get.id',0,'intval');
		if($id == 0){
			$this->error('参数传输错误');
		}else{
			if(M("Goods_cate")->delete($id)){
				$this->success('删除成功',U('showlist'));
			}else{
				$this->error('删除失败，'.M("Goods_cate")->getError());
			}
		}
    }
}